package com.okadatech.sample.recyclerview.view.activity;

import android.app.LauncherActivity;
import android.support.annotation.NonNull;

import com.okadatech.sample.recyclerview.R;

import java.util.ArrayList;
import java.util.List;


public final class MenuActivity extends LauncherActivity {

    @NonNull
    @Override
    public List<ListItem> makeListItems() {
        final List<ListItem> items = new ArrayList<>();

        // StaggeredGridLayoutManager
        final ListItem itemStaggeredGridLayoutManager = new ListItem();
        itemStaggeredGridLayoutManager.className = StaggeredGridLayoutManagerActivity.class
                .getName();
        itemStaggeredGridLayoutManager.label = "StaggeredGridLayoutManager";
        items.add(itemStaggeredGridLayoutManager);

        for (final ListItem item : items) {
            if (item.icon == null) {
                item.icon = getResources().getDrawable(R.drawable.ic_launcher);
            }
            if (item.packageName == null) {
                item.packageName = getPackageName();
            }
        }

        return items;
    }
}
