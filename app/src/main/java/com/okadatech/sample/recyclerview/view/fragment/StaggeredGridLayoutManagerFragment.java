package com.okadatech.sample.recyclerview.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.okadatech.sample.recyclerview.R;
import com.okadatech.sample.recyclerview.view.adapter.StaggeredGridLayoutManagerAdapter;

public final class StaggeredGridLayoutManagerFragment
        extends Fragment {
    @SuppressWarnings("unused")
    private static final String TAG = StaggeredGridLayoutManagerFragment.class.getSimpleName();

    private static final int SPAN_COUNT = 4;

    public static StaggeredGridLayoutManagerFragment newInstance() {
        return new StaggeredGridLayoutManagerFragment();
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_staggered_grid_layout_manager, container, false);
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(
                new StaggeredGridLayoutManager(SPAN_COUNT, StaggeredGridLayoutManager.VERTICAL));
        recyclerView.setAdapter(new StaggeredGridLayoutManagerAdapter());
    }
}
