package com.okadatech.sample.recyclerview.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.okadatech.sample.recyclerview.R;

import java.util.Date;
import java.util.Random;

public final class StaggeredGridLayoutManagerAdapter
        extends RecyclerView.Adapter<StaggeredGridLayoutManagerAdapter.AbstractViewHolder> {
    private static final String TAG = StaggeredGridLayoutManagerAdapter.class.getSimpleName();

    private static final int[] VIEWS = {
            R.layout.item_staggered_grid_layout_manager_2,
            R.layout.item_staggered_grid_layout_manager_3,
            R.layout.item_staggered_grid_layout_manager_4};

    private static final int COUNTS = 100;

    private final int[] mItems = new int[COUNTS];

    public StaggeredGridLayoutManagerAdapter() {
        final Random random = new Random(new Date().getTime());
        for (int i = 0; i < COUNTS; ++i) {
            mItems[i] = VIEWS[random.nextInt(VIEWS.length)];
        }
    }

    @Override
    public int getItemCount() {
        return mItems.length;
    }

    @Override
    public int getItemViewType(final int position) {
        return mItems[position];
    }

    @Override
    public AbstractViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int viewType) {
        final View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(viewType, viewGroup, false);

        switch (viewType) {
            case R.layout.item_staggered_grid_layout_manager_2:
                return new ViewHolder2(v);

            case R.layout.item_staggered_grid_layout_manager_3:
                return new ViewHolder3(v);

            case R.layout.item_staggered_grid_layout_manager_4:
                return new ViewHolder4(v);

            default:
                Log.e(TAG, "unknown view type : " + viewType);
                return null;
        }
    }

    @Override
    public void onBindViewHolder(final AbstractViewHolder viewHolder, final int position) {
        viewHolder.mTextView.setText(String.valueOf(position));
    }

    static class AbstractViewHolder extends RecyclerView.ViewHolder {

        private final TextView mTextView;

        public AbstractViewHolder(final View itemView) {
            super(itemView);

            mTextView = (TextView) itemView.findViewById(R.id.text_view);
        }
    }

    private static final class ViewHolder2
            extends AbstractViewHolder {

        public ViewHolder2(final View itemView) {
            super(itemView);
        }
    }

    private static final class ViewHolder3
            extends AbstractViewHolder {

        public ViewHolder3(final View itemView) {
            super(itemView);
        }
    }

    private static final class ViewHolder4
            extends AbstractViewHolder {

        public ViewHolder4(final View itemView) {
            super(itemView);
        }
    }
}
