package com.okadatech.sample.recyclerview.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.okadatech.sample.recyclerview.view.fragment.StaggeredGridLayoutManagerFragment;


public class StaggeredGridLayoutManagerActivity extends ActionBarActivity {
    @SuppressWarnings("unused")
    private static final String TAG = StaggeredGridLayoutManagerActivity.class.getSimpleName();

    public static void start(final Activity activity) {
        activity.startActivity(new Intent(activity, StaggeredGridLayoutManagerActivity.class));
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // workaround for ActionBarActivity's bug (v21 or older)
        getSupportActionBar();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(android.R.id.content, StaggeredGridLayoutManagerFragment.newInstance())
                    .commit();
        }
    }
}
